# scoop-java

[![Mentioned in Awesome Scoop](https://awesome.re/mentioned-badge.svg)](https://github.com/scoopinstaller/awesome-scoop)

A bucket for [Scoop](https://scoop.sh), for [BlueJ](https://www.bluej.org/) and [Greenfoot](https://www.greenfoot.org/).

To make it easy to install apps from this bucket, run
    `scoop bucket add wgs-java https://gitlab.com/wgs-buckets/java.git`

For more information, read the [wiki](https://github.com/lukesampson/scoop/wiki/Java).
